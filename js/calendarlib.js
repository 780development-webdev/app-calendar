/**
	780Development
	Giakhanh Hoang
	
 */

function calendarGetURLRequest(request) {
	switch(request) {
		case 'newUserCheck':
			return 'sv/checkUser.php';
			break;
		case 'retrieveCalendars':
			return 'sv/getCalendars.php';
			break;
		case 'createCalendar':
			return 'sv/createCalendar.php';
			break;
		case 'fillEventInfo':
			return 'sv/getEventInfo.php';
			break;
		case 'getEventInfo':
			return 'sv/getEventInfo.php';
			break;
		case 'getEventsList':
			return 'sv/getEvents.php';
			break;
		case 'shareCalendarInfo':
			return 'sv/userShareCalendar.php';
			break;
		default:
			return 'NaN';
			break;
	}
}

function onCalendarNetRequestPass(request, data, response) {
	switch(request) {
		case 'newUserCheck':
			newUserCheck(response);
			calendarNetRequest('retrieveCalendars');
			break;
		case 'retrieveCalendars':
			retrieveCalendars(response);
			break;
		case 'createCalendar':
			createCalendar(response);
			break;
		case 'fillEventInfo':
			fillEventInfo(response);
			break;
		case 'getEventInfo':
			getEventInfo(response);
			break;
		case 'getEventsList':
			getEventsList(response);
			break;
		case 'shareCalendarInfo':
			shareCalendarResult(response);
			break;
		default:
			return 'NaN';
			break;
	}
}

function calendarNetRequest(request, data) {
	var url = calendarGetURLRequest(request);
	if(url == 'NaN') {
		return;
	}
	var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
		if(this.readyState == 4 && this.status == 200) {
			var responseText = this.responseText;
			//console.log(responseText);
			onCalendarNetRequestPass(request, data, responseText);
		} else {
			console.log(responseText);
		}
    }
    xmlhttp.open('POST', url, true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.send(data);
}

function newUserCheck(response) {
	if(response == '//CALENDAR_CREATE_USER_SUCCESS' || response == '//CALENDAR_USER_EXISTS') {
		document.getElementById('loading-progress').style.width = '66%';
	} 
}

function retrieveCalendars(response) {
	var responseArr = response.split('\n');
    var calendarListHTML = '';
    for(var i = 0; i < responseArr.length-1; i = i+7) {
        console.log('ID: ' + responseArr[i] + '\nFull ID: ' + responseArr[i+5] + '\nName: ' + responseArr[i+2] + '\nAuthorized: ' + responseArr[i+3] + '\nNumber of Events: ' + responseArr[i+4]);
        calendarListHTML += '<tr>' +
            '<td>' + responseArr[i+2] + '</td>' +
            '<td>' + responseArr[i+4] + '</td>' +
            '<td><button class=\'btn btn-primary\' onclick=\'viewEventList(&#39;' + responseArr[i+5] + '&#39;, ' + '&#39;' + responseArr[i+2] + '&#39;)\'>View Events</button>' +
            '<button class=\'btn btn-secondary\' onclick=\'viewOptions(&#39;' + responseArr[i+5] + '&#39;, ' + responseArr[i+4] + ', ' + '&#39;' + responseArr[i+2] + '&#39;,&#39;' + responseArr[i+1] + '&#39;)\' style=\'margin-left:12px\'>Options</button>' +
            '</td></tr>';
    }
	document.getElementById('calendar_list').innerHTML = calendarListHTML;
	document.getElementById('loading-progress').style.width = '100%';
	document.getElementById('loading-div').style.display = 'none';
}

function createCalendar(response) {
	if(response != '//CALENDAR_CREATE_SUCCESS') {
        document.getElementById('serverResponse').innerHTML = response;
    } else {
        window.location = 'home.html';
	}
}

function fillEventInfo(response) {
	var responseArr = response.split('\n');
    console.log('Event ID: ' + responseArr[0] + 
            '\nUser: ' + responseArr[1] + 
            '\nDate Start: ' + responseArr[2] + 
            '\nDate End: ' + responseArr[3] + 
            '\nEvent Name: ' + responseArr[4] + 
            '\nEvent Description: ' + responseArr[5] + 
            '\nPriority: ' + responseArr[6]);
    var begDate = convertSQLTimeToJSTime(responseArr[2]);
    var endDate = convertSQLTimeToJSTime(responseArr[3]);
    document.getElementById('event_id').value = responseArr[0];
    document.getElementById('event_start_date').value = formatToDateTimeInput(begDate);
    document.getElementById('event_end_date').value = formatToDateTimeInput(endDate); 
    document.getElementById('event_name').value = responseArr[4];
    document.getElementById('event_description').value = responseArr[5];
    document.getElementById('event_priority').value = responseArr[6];
}

function getEventInfo(response) {
	var responseArr = response.split('\n');
    var eventInfoHTML = '';
    console.log('Event ID: ' + responseArr[0] + 
                '\nUser: ' + responseArr[1] + 
                '\nDate Start: ' + responseArr[2] + 
                '\nDate End: ' + responseArr[3] + 
                '\nEvent Name: ' + responseArr[4] + 
                '\nEvent Description: ' + responseArr[5] + 
                '\nPriority: ' + responseArr[6]);
    eventInfoHTML = 'Start Date: ' + responseArr[2] + '<br>' +
                    'End Date: ' + responseArr[3] + '<br><br>' +
                    'Author: ' + responseArr[1] + '<br><br>' +
                    'Description: <br>' + responseArr[5] + '<br><br>' +
                    'Priority: ' + responseArr[6];
    document.getElementById('event_header').innerHTML = responseArr[4];
    document.getElementById('event_body').innerHTML = eventInfoHTML;
	document.getElementById('page_tab_event_title').innerHTML = responseArr[4] + ' - Event Info';
}

function getEventsList(response) {
	var responseArr = response.split('\n');
    var eventListHTML = '';
    for(var i = 0; i < responseArr.length-1; i = i+8) {
        console.log('Event ID: ' + responseArr[i] + 
                    '\nUser: ' + responseArr[i+1] + 
                    '\nDate Start: ' + responseArr[i+2] + 
                    '\nDate End: ' + responseArr[i+3] + 
                    '\nEvent Name: ' + responseArr[i+4] + 
                    '\nEvent Description: ' + responseArr[i+5] + 
                    '\nPriority: ' + responseArr[i+6]);
        var begDateParts = responseArr[i+2].split(/[- :]/);
        var endDateParts = responseArr[i+3].split(/[- :]/);
        var dateStart = convertSQLTimeToJSTime(responseArr[i+2]);
        var dateEnd = convertSQLTimeToJSTime(responseArr[i+3]);
        eventListHTML += '<tr>' +
            '<td>' + formatDateRange(dateStart, dateEnd, false) + '</td>' +
            '<td><a href=\'#\' onclick=\'viewEventInfo(' + responseArr[i] + ')\'>' + responseArr[i+4] + '</a></td>' +
            '<td>' + responseArr[i+5] + '</td>' +
            '<td>' + responseArr[i+6] + '</td>' +
            '</tr>';
    }
    document.getElementById('events_list').innerHTML = eventListHTML;
    document.getElementById('loading-progress').style.width = '100%';
    document.getElementById('loading-div').style.display = 'none';
}

function shareCalendarResult(response){
	if(response != '//CALENDAR_SHARE_CALENDAR_INFO_SUCCESS') {
        document.getElementById('serverResponse').innerHTML = response;
    } else {
        window.location = 'home.html';
	}
}

/****************************************
	Non-network related functions below
 ****************************************/
 function validateEventCreationInput() {
	var begDateInput = document.getElementById('event_start_date').value;
	var endDateInput = document.getElementById('event_end_date').value;
	var eventNameInput = document.getElementById('event_name').value;
	var eventDescInput = document.getElementById('event_description').value;
	
	console.log("Here's what I have: " + begDateInput + " " + endDateInput + " " + eventNameInput + " " + eventDescInput);
	var begDateObj;
	var endDateObj;
	
	if(begDateInput != "") {
		if(begDateInput.length < 11) {
			begDateInput += " 00:00:00";
		} else {
			begDateInput = begDateInput.split("T")[0] + " " + begDateInput.split("T")[1] + ":00";
		}
		console.log(begDateInput);
		begDateObj = convertSQLTimeToJSTime(begDateInput); 
	}
	if(endDateInput != "") {
		if(endDateInput.length < 11) {
			endDateInput += " 00:00:00";
		} else {
			endDateInput = endDateInput.split("T")[0] + " " + endDateInput.split("T")[1] + ":00";
		}
		console.log(endDateInput);
		endDateObj = convertSQLTimeToJSTime(endDateInput);
	}
	var nowDateObj = new Date();
		console.log("Log: " + begDateObj.getTime() + " " + endDateObj.getTime() + " " + nowDateObj.getTime());
	if(begDateInput == "" || endDateInput == "" || eventNameInput == "" || eventDescInput == "") {
		document.getElementById('sys-feedback').innerHTML = 'One or more field(s) is empty';
		console.log("One or more field(s) is empty");
	} else if(begDateObj.getTime() < nowDateObj.getTime() && endDateObj.getTime() < nowDateObj.getTime()) {
		document.getElementById('sys-feedback').innerHTML = 'Cannot create an event in the past at this time';
	} else if(endDateObj.getTime() < begDateObj.getTime()) {
		document.getElementById('sys-feedback').innerHTML = 'Cannot create a paradox in time';
	} else {
		document.getElementById('eventForm').submit();
	}
 }
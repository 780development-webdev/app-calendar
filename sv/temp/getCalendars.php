<?php
	include "/var/wkeys/skey.php";
	
	if(!isset($_COOKIE['web_userToken'])) {
		die("//CALENDAR_USER_UNKNOWN");
	} else {
		$username = $_COOKIE['web_username'];
	}
	
	if(!mysqli_connect(HOST, USER, PASS, 'calendar')) {
		die("//CALENDAR_DB_CONNECT_FAIL");
	} else {
		$con = mysqli_connect(HOST, USER, PASS, 'calendar');
	}
	
	#Query events list
	$cmd1 = "SELECT * FROM calendars_$username ORDER BY calendar_id DESC;";
	$value1_0 = mysqli_query($con, $cmd1);
	if($value1_1 = mysqli_fetch_array($value1_0)) {			#Checks that the array returns something
		$value1_0 = mysqli_query($con, $cmd1);				#Reset value1_0 so value1_1's query will start at the beginning
		while($value1_1 = mysqli_fetch_array($value1_0)) {	#Iterate through the entire table and echo the news
			echo $value1_1['calendar_id']."\n";				#i
			echo $value1_1['owner']."\n";					#i+1
			echo $value1_1['calendar_name']."\n";			#i+2
			echo $value1_1['authorized_users']."\n";		#i+3
			echo $value1_1['num_events']."\n";				#i+4
			echo $value1_1['full_calendar_id']."\n";		#i+5
			echo "//!ENDTRACK//"."\n";						#i+6
		}
		echo "//CALENDAR_RETRIEVAL_SUCCESS";					
	} else {
		echo "//CALENDAR_RETRIEVAL_FAIL";				
	}
	mysqli_close($con);
?>
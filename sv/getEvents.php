<?php
	include "/var/wkeys/skey.php";
	
	if(!mysqli_connect(HOST, USER, PASS, 'calendar')) {
		die("//CALENDAR_DB_CONNECT_FAIL");
	} else {
		$con = mysqli_connect(HOST, USER, PASS, 'calendar');
	}
	
	$username = $_COOKIE['web_username'];
	#$calendarID = $_COOKIE['calendar_id'];
	$fullCalendarID = $_COOKIE['full_calendar_id'];
	
	#Query events list
	#$cmd1 = "SELECT * FROM calendar_$username"."_$calendarID ORDER BY date_start DESC;";
	$cmd1 = "SELECT * FROM calendar_$fullCalendarID ORDER BY date_start DESC;";
	$value1_0 = mysqli_query($con, $cmd1);
	if($value1_1 = mysqli_fetch_array($value1_0)) {			#Checks that the array returns something
		$value1_0 = mysqli_query($con, $cmd1);				#Reset value1_0 so value1_1's query will start at the beginning
		while($value1_1 = mysqli_fetch_array($value1_0)) {	#Iterate through the entire table and echo the news
			echo $value1_1['event_id']."\n";
			echo $value1_1['user']."\n";
			echo $value1_1['date_start']."\n";				#RL01
			echo $value1_1['date_end']."\n";				#RL02
			echo $value1_1['event_name']."\n";				#RL03
			echo $value1_1['event_desc']."\n";				#RL04
			echo $value1_1['priority_flag']."\n";			#RL05
			echo "//!ENDTRACK//"."\n";						#RL06
		}
		echo "//CALENDAR_EVENTS_RETRIEVAL_SUCCESS";			#RL07
	} else {
		echo "//CALENDAR_EVENTS_RETRIEVAL_FAIL";			#RL08
	}
	mysqli_close($con);
?>
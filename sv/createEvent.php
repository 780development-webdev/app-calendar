<?php
	include "/var/wkeys/skey.php";
	
	// Event Information from submitted form
	$begDate = $_POST['begDate'];
	$endDate = $_POST['endDate'];
	$pritNum = $_POST['pritNum'];
	$eName = $_POST['eName'];
	$eDesc = $_POST['eDesc'];
	
	// Token should already be validated.
	$username = $_COOKIE['web_username'];
	$fullCalendarID = $_COOKIE['full_calendar_id'];
	
	if(!mysqli_connect(HOST, USER, PASS, 'calendar')) {
		die("//CALENDAR_DB_CONNECT_FAIL");
	} else {
		$con = mysqli_connect(HOST, USER, PASS, 'calendar');
	}
	
	$cmd1 = mysqli_prepare($con, "INSERT INTO calendar_$fullCalendarID (user, date_start, date_end, event_name, event_desc, priority_flag) VALUES (?, ?, ?, ?, ?, ?);");
	mysqli_stmt_bind_param($cmd1, "sssssi", $username, $begDate, $endDate, $eName, $eDesc, $pritNum);
	if(mysqli_stmt_execute($cmd1)) {
		header('Location: ../eventsList.html');
	} else {
		echo "//CALENDAR_EVENT_ADD_FAIL";
	}
	mysqli_stmt_close($cmd1);
	mysqli_close($con);
?>
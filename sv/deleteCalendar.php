<?php
	include "/var/wkeys/skey.php";
	
	// Token should already be validated.
	$username = $_COOKIE['web_username'];
	$fullCalendarID = $_COOKIE['full_calendar_id'];
	
	if(!mysqli_connect(HOST, USER, PASS, 'calendar')) {
		die("//CALENDAR_DB_CONNECT_FAIL");
	} else {
		$con = mysqli_connect(HOST, USER, PASS, 'calendar');
	}
	
	$cmd1 = "DROP TABLE calendar_$fullCalendarID";
	if(mysqli_query($con, $cmd1)) {			
		$cmd2 = "DELETE FROM calendars_$username WHERE full_calendar_id = '$fullCalendarID'";
		if(mysqli_query($con, $cmd2)) {
			header('Location: ../home.html');
		} else {
			echo "//CALENDAR_INFO_DELETION_FAIL";
		}
	} else {
		echo "//CALENDAR_DELETION_FAIL";
	}
	mysqli_close($con);
?>
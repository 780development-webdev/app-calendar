<?php
	include "/var/wkeys/skey.php";
	
	// Event Information from submitted form
	$begDate = $_POST['begDate'];
	$endDate = $_POST['endDate'];
	$pritNum = $_POST['pritNum'];
	$eName = $_POST['eName'];
	$eDesc = $_POST['eDesc'];
	$eID = $_POST['eid'];
	$eDelete = $_POST['eDelete'];
	
	// Token should already be validated.
	$username = $_COOKIE['web_username'];
	$fullCalendarID = $_COOKIE['full_calendar_id'];
	
	if(!mysqli_connect(HOST, USER, PASS, 'calendar')) {
		die("//CALENDAR_DB_CONNECT_FAIL");
	} else {
		$con = mysqli_connect(HOST, USER, PASS, 'calendar');
	}
	if($eDelete) {
		$cmd1 = mysqli_prepare($con, "DELETE FROM calendar_$fullCalendarID WHERE event_id = ?");
		mysqli_stmt_bind_param($cmd1, "i", $eID);
	} else {
		$cmd1 = mysqli_prepare($con, "UPDATE calendar_$fullCalendarID SET date_start = ?, date_end= ?, event_name = ?, event_desc = ?, priority_flag = ? WHERE event_id = ?");
		mysqli_stmt_bind_param($cmd1, "ssssii", $begDate, $endDate, $eName, $eDesc, $pritNum, $eID);
	}
	
	if(mysqli_stmt_execute($cmd1)) {
		header('Location: ../eventsList.html');
	} else {
		echo "//CALENDAR_EVENT_UPDATE_FAIL";
	}
	mysqli_stmt_close($cmd1);
	mysqli_close($con);
?>
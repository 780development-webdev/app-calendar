<?php
	include "/var/wkeys/skey.php";
	#include "calendarFunc.php";
	
	$newCalendarName = $_POST['newCalendarName'];
	
	// Token should already be validated.
	$username = $_COOKIE['web_username'];
	
	if(!mysqli_connect(HOST, USER, PASS, 'calendar')) {
		die("//CALENDAR_DB_CONNECT_FAIL");
	} else {
		$con = mysqli_connect(HOST, USER, PASS, 'calendar');
	}
	
	$cmd1 = "INSERT INTO calendars_$username (owner, calendar_name) VALUES ('$username', '$newCalendarName');";
	if(mysqli_query($con, $cmd1)) {			
		$cmd2 = "SELECT calendar_id FROM calendars_$username WHERE calendar_name='$newCalendarName';";
		$value2_0 = mysqli_query($con, $cmd2);
		if($value2_1 = mysqli_fetch_array($value2_0)) {
			$calendarID = $value2_1[0];
			$cmd3 = "CREATE TABLE calendar_$username"."_$calendarID (event_id INT(4) AUTO_INCREMENT, user VARCHAR(20), date_start DATETIME, date_end DATETIME, event_name VARCHAR(64), event_desc VARCHAR(256), priority_flag INT(4), PRIMARY KEY (event_id));";
			if(mysqli_query($con, $cmd3)) {
				$cmd4 = "UPDATE calendars_$username SET full_calendar_id = '$username"."_$calendarID' WHERE calendar_id='$calendarID';";
				if(mysqli_query($con, $cmd4)) {
					echo "//CALENDAR_CREATE_SUCCESS";
				} else {
					echo "//CALENDAR_CREATE_ID_UPDATE_FAIL";
				}
			} else {
				echo "//CALENDAR_CREATE_TABLE_FAIL";
			}
		} else {
			echo "//CALENDAR_ID_RETR_FAIL";
		}
	} else {
		echo "//CALENDAR_CREATE_FAIL";
	}
	mysqli_close($con);
?>